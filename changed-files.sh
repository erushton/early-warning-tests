#!/bin/bash

SHA=`git rev-parse HEAD`
CHANGED=`git diff-tree --no-commit-id --name-only -r ${SHA}`

for f in $CHANGED
do
    echo "$f was changed. Running tests for it"
    ftest=`echo $f | sed 's/\.go/_test\.go/'`
    c="go test -v ${ftest} ${f}"
    echo $c
    $c
done
