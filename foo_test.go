package foo

import "testing"

func TestFoo(t *testing.T) {
    hopefullyFoo := Foo()
    if hopefullyFoo != "foo" {
        t.Errorf("Not foo but I wish it was: %v", hopefullyFoo)
    }
}
