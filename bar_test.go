package foo 

import "testing"

func TestBar(t *testing.T) {
    hopefullyBar := Bar()
    if hopefullyBar != "bar" {
        t.Errorf("Bar() = %s; but want 'bar'", hopefullyBar)
    }
}
